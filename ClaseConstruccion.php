<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ClaseConstruccion
 *
 * @author Hector
 */
class ClaseConstruccion {
    
     //Atriburos de la clase
        private $nombre;
        private $ubicacion;
        private $terreno;
        private $estado;
        private $area;
        private $altura;
        private $tipoconstrucc;
        private $resistencia;
        private $bases;
        private $licencias;
        private $destinacion;
        private $presupuesto;
        private $impuestos;
        private $legalizaciones;
        
        //Métoodos de la clase
        public function __construct()
        {
            $this->nombre = "Torres de serranías 5 etapas";
            $this->ubicacion = "santa fé de Antioquia";
            $this->terreno = "Un poco arcilloso";
            $this->estado = "Un poco valdío el terreno";
            $this->area = 5000;
            $this->altura = 6.95;
            $this->tipoconstrucc = "Urbanística";
            $this->resistencia = "Es sismoresistente";
            $this->bases = 15;
            $this->licencias = "Completas";
            $this->destinacion = "Inicialmente viviend, comercio y turismo";
            $this->presupuesto = 17000000000;
            $this->impuestos = "Al día";
            $this->legalizaciones = "En trámite";
            
        }
        
        public function _destruct()
        {
            
        }
        
        function getNombre() {
            return $this->nombre;
        }

        function getUbicacion() {
            return $this->ubicacion;
        }

        function getTerreno() {
            return $this->terreno;
        }

        function getEstado() {
            return $this->estado;
        }

        function getArea() {
            return $this->area;
        }

        function getAltura() {
            return $this->altura;
        }

        function getTipoconstrucc() {
            return $this->tipoconstrucc;
        }

        function getResistencia() {
            return $this->resistencia;
        }

        function getBases() {
            return $this->bases;
        }

        function getLicencias() {
            return $this->licencias;
        }

        function getDestinacion() {
            return $this->destinacion;
        }

        function getPresupuesto() {
            return $this->presupuesto;
        }

        function getImpuestos() {
            return $this->impuestos;
        }

        function getLegalizaciones() {
            return $this->legalizaciones;
        }

        function setNombre($nombre) {
            $this->nombre = $nombre;
        }

        function setUbicacion($ubicacion) {
            $this->ubicacion = $ubicacion;
        }

        function setTerreno($terreno) {
            $this->terreno = $terreno;
        }

        function setEstado($estado) {
            $this->estado = $estado;
        }

        function setArea($area) {
            $this->area = $area;
        }

        function setAltura($altura) {
            $this->altura = $altura;
        }

        function setTipoconstrucc($tipoconstrucc) {
            $this->tipoconstrucc = $tipoconstrucc;
        }

        function setResistencia($resistencia) {
            $this->resistencia = $resistencia;
        }

        function setBases($bases) {
            $this->bases = $bases;
        }

        function setLicencias($licencias) {
            $this->licencias = $licencias;
        }

        function setDestinacion($destinacion) {
            $this->destinacion = $destinacion;
        }

        function setPresupuesto($presupuesto) {
            $this->presupuesto = $presupuesto;
        }

        function setImpuestos($impuestos) {
            $this->impuestos = $impuestos;
        }

        function setLegalizaciones($legalizaciones) {
            $this->legalizaciones = $legalizaciones;
        }
        
        
        public function actualizarAltura()
        { 
            $this->altura = 18.85;
        }

                public function actualizarPresupuesto()
    {
        $this->presupuesto = 2500000000;
    }
    
    public function Destinacion ($destinacion)
    {
        switch ($destinacion){
            case "urbana":
                $this->destinacion = "casa unifamiliar";
                break;
            case "rural":
                $this->destinacion = "casa veredal";
                break;
            case "comercial":
                $this->destinacion = "locales y oficinas";
                break;
            case "interessocial":
                $this->destinacion = "subsidio de vivienda";
                break;
            }
        }
        
        public function acutalizarLegalizaciones()
        {  
           $this->legalizaciones = "completas";
        }
    }
    
        
        
        
        
        
       
 

