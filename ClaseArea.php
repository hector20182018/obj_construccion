<?php


/**
 * Description of ClaseArea
 *
 * @author Hector
 */


class ClaseArea 

{
    
    private $largo;
    private $ancho;
    private $perimetro;
    private $descripcion;
    private $areaactual;
    private $perimactual;


    public function _construct()
    {
        $this->largo =100;
        $this->ancho=50;
        $this->perimetro=200;
        $this->descripcion="";
        $this->areaactual = 5000;
        $this->perimactual =0;
    }
    
    public function _destruct()
    {
        
    }
       
    function getLargo() {
        return $this->largo;
    }

    function getAncho() {
        return $this->ancho;
    }

    function getPerimetro() {
        return $this->perimetro;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function getAreaactual() {
        return $this->areaactual;
    }

    function getPerimactual() {
        return $this->perimactual;
    }

    function setLargo($largo) {
        $this->largo = $largo;
    }

    function setAncho($ancho) {
        $this->ancho = $ancho;
    }

    function setPerimetro($perimetro) {
        $this->perimetro = $perimetro;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    function setAreaactual($areaactual) {
        $this->areaactual = $areaactual;
    }

    function setPerimactual($perimactual) {
        $this->perimactual = $perimactual;
    }

     public function actualizarArea($descripcion)
    {
       if(($this->largo * $this->ancho)>= 5000)
       {
           $this->descripcion = "El área es mayor a la inicial";
       }
       else 
     {
        $this->descripcion = "El área es menor a la inicial";   
           
       }
    }
  }

   


