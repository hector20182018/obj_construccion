<?php


/**
 * Description of ClaseArea
 *
 * @author Hector
 */


class ClaseArea 

{
    
    private $largo;
    private $ancho;
    private $perimetro;
    private $descripcion;
    private $descripcion2;
    private $areainicial;
    private $areaactual;
    private $perimactual;
        


    public function _construct()
    {
        $this->largo =100;
        $this->ancho=50;
        $this->perimetro=200;
        $this->descripcion="";
        $this->descripcion2="";
        $this->areaactual = 0;
        $this->areainicial = 5000;
        $this->perimactual =0;
    }
    
    public function _destruct()
    {
        
    }
       
    function getLargo() {
        return $this->largo;
    }

    function getAncho() {
        return $this->ancho;
    }

    function getPerimetro() {
        $this->perimetro = 200;
        return $this->perimetro;
    }

    function getDescripcion() {
        return $this->descripcion;
    }
    
    function getDescripcion2() {
        return $this->descripcion2;
    }
    
    function getAreainicial() {
         $this->areainicial = 5000;
        return $this->areainicial;
    }

    function getAreaactual() {
       return $this->areaactual;
    }

    function getPerimactual() {
        return $this->perimactual;
    }

    function setLargo($largo) {
        $this->largo = $largo;
    }

    function setAncho($ancho) {
        $this->ancho = $ancho;
    }

    function setPerimetro($perimetro) {
        $this->perimetro = $perimetro;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }
    
    function setDescripcion2($descripcion2) {
        $this->descripcion2 = $descripcion2;
    }


    function setAreaactual($areaactual) {
        $this->areaactual = $areaactual;
    }

    function setPerimactual($perimactual) {
        $this->perimactual = $perimactual;
    }

     public function actualizarArea($areaactual)
    {
       
         $this->areaactual = $this->largo * $this->ancho;
       
         if($this->areaactual >= 5000)
         {
             $this->descripcion = " el área es bien grande ";
         }
        else {
            $this->descripcion = " el área  es más bién pequeña ";
        }
        return $this->areaactual; 
               }
        
        public function actualizarPerimetro($perimactual)
    {
       
         $this->perimactual = (($this->largo *2)+($this->ancho * 2));
         
         if($this->perimactual >= 250)
         {
             $this->descripcion2 = " tiene muy buen perímetro ";
         }
        else {
            $this->descripcion2 = " el perímetro también";
        }
        return $this->perimactual;
         
        }
  }

   


